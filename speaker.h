 /*
  * This file defines the functions for controlling the speaker
  */ 
  
#include "notes.h"
#include <stdlib.h> // malloc, free
#include <stdbool.h>

// Tone - Version: Latest 
#include <Tone.h>

#pragma once

// Create a string type for
typedef char* string;

// Create a linked list in order to nicely refer to speakers
struct speaker {
  short pin;
  Tone *tone;
  string name;
  struct speaker *next;
} speaker;

// Get all existing speakers;
struct speaker *initializeSpeakers();

static struct speaker *speakers = initializeSpeakers();

// Create a speaker with the aforementioned information
static struct speaker *createSpeaker(short pin, string name, struct speaker *next) {
  // Create the speaker struct
  struct speaker *speaker = malloc(sizeof(struct speaker));

  // Give it the right information
  speaker->pin = pin;
  speaker->name = name;
  speaker->tone = new Tone;
  speaker->tone->begin(pin);
  
  // Point to the next one in the sequence
  speaker->next = next;

  return speaker;
}

// Search for the speaker using a name
static struct speaker *searchSpeakerHelper(const string name,  struct speaker *speakers) {
  struct speaker *current = speakers;

  // Check if this is the right speaker and if so return it
  if (strcmp(name, current->name) == 0)
    return current;
  // Return a NULL if you can't find the right speaker to prevent an infinite loop
  else if (current->next == NULL)
    return NULL;
  else
    // Return whether or not the next in the list is the right speaker
    // OPTIMIZATION: Iteration and not recursion
    return searchSpeakerHelper(name, current->next);
}

struct speaker *searchSpeaker(const string name) {
  return searchSpeakerHelper(name, speakers);
}

// Create the list of speakers in reverse order and return the last one
// This allows for the 
struct speaker *initializeSpeakers() {
  struct speaker *infrarood = createSpeaker(10, "infrarood", NULL);
  struct speaker *sonar = createSpeaker(3, "sonar", infrarood);
  struct speaker *temperatuur = createSpeaker(9, "temperatuur", sonar);
  return temperatuur;
}

// Free the memory for all the speakers in the list
// OPTIMIZATION: iteration instead of recursion
static void clearSpeakersHelper(struct speaker *speaker) {
  // First queue up all the speakers we know of
  if (speaker->next != NULL)
    clearSpeakersHelper(speaker->next);

  // Then free them in order
  free(speaker);

  return;
}

void clearSpeakers() {
  clearSpeakersHelper(speakers);
}

// Internal definition
static void playNoteBase(struct note cnote, int baseDuration, string speakerName) {
  struct speaker *speaker = searchSpeaker(speakerName);

  speaker->tone->play(cnote.freq, baseDuration / cnote.duration);
}

void playNote(struct note cnote, string speakerName) {
  playNoteBase(cnote, 3000, speakerName);
}

void playRandomNote() {
  short row = random(0, 7);
  string note;
  
  if (row == 0)
    note = notes[0][0];
  else if (row == 7)
    note = notes[7][random(0, 3)];
  else
    note = notes[row][random(0,11)];
  
  Serial.println(note);
  playNoteBase(createNote(NULL, note, 1), 3000, "infrarood"); // correct length?
}