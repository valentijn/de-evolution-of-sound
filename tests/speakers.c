#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef char* string;

struct speaker {
    short pin;
    string name;
    struct speaker *next;
} note;

// Create a speaker with the aforementioned information
struct speaker *createSpeaker(short pin, string name, struct speaker *next) {
    struct speaker *speaker = malloc(sizeof(struct speaker));

    speaker->pin = pin;
    speaker->name = name;
    speaker->next = next;

    return speaker;
}

struct speaker *searchSpeaker(const string name,  struct speaker *speakers) {
    struct speaker *current = speakers;

    if (strcmp(name, current->name) == 0)
        return current;
    else if (current->next == NULL)
        return NULL;
    else
        return searchSpeaker(name, current->next);
}

struct speaker *initializeSpeakers() {
    struct speaker *infrarood = createSpeaker(6, "infrarood", NULL);
    struct speaker *sonar = createSpeaker(9, "sonar", infrarood);
    return sonar;
}

void clearSpeakers(struct speaker *speaker) {
    if (speaker->next != NULL)
        clearSpeakers(speaker->next);

    free(speaker);

    return;
}

int main(void)
{
    struct speaker *speakers = initializeSpeakers();
    struct speaker *infrarood = searchSpeaker("sonar", speakers);

    printf("%s - %d\n", infrarood->name, infrarood->pin);
    clearSpeakers(speakers);
}
