/*
 * Rather boring file containing the pins and the function prototypes
 */ 
#include <ST_HW_HC_SR04.h>
#include <stdbool.h> // true, false
#include <stdlib.h> // atexit
#include "notes.h"
#include "speaker.h"

#pragma once

// Constant values for the pins
const int infraroodPin   = 2;
const int temperatuurPin = A1;
const int lichtPin       = A2;
const int echoPin        = 12;
const int triggerPin     = 13;
const int LEDPin         = 5;

// Global variables
int lichtValue; // Only light is used in multiple functions 
ST_HW_HC_SR04 ultrasonicSensor(triggerPin, echoPin); // Special type for the sonar

// Sensor functions
void runInfrarood(int infraroodValue);
void runTemperatuur(int temperatuurValue);
void runSonar(int hitTime);
