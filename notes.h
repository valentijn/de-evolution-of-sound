 /*
  * This is the file that defines the functions for creating notes.
  * Notes are structures with a frequency, a character representation and the duration.
 */

#include <assert.h> // assert
#include <stdbool.h> // true, false

// Only include the library once
#pragma once

// Write macro to select both a sharp and non-sharp note
#define SHARP(x) (sharp ? x + 1 : x)

// Notes arrays

// Define a string type for easy reading.
typedef char* string;

// The main structure we're going to use
struct note {
  short freq;
  string note;
  short duration;
} note;

// Arrays
const string notes[9][12] = {
  {"B0"},
  {"C1", "CS1", "D1", "DS1", "E1", "F1", "FS1", "G1", "GS1", "A1", "AS1", "B1"},
  {"C2", "CS2", "D2", "DS2", "E2", "F2", "FS2", "G2", "GS2", "A2", "AS2", "B2"},
  {"C3", "CS3", "D3", "DS3", "E3", "F3", "FS3", "G3", "GS3", "A3", "AS3", "B3"},
  {"C4", "CS4", "D4", "DS4", "E4", "F4", "FS4", "G4", "GS4", "A4", "AS4", "B4"},
  {"C5", "CS5", "D5", "DS5", "E5", "F5", "FS5", "G5", "GS5", "A5", "AS5", "B5"},
  {"C6", "CS6", "D6", "DS6", "E6", "F6", "FS6", "G6", "GS6", "A6", "AS6", "B6"},
  {"C7", "CS7", "D7", "DS7", "E7", "F7", "FS7", "G7", "GS7", "A7", "AS7", "B7"},
  {"C8", "CS8", "D8", "DS8"}
};

const short freqs[9][12] = {
  {31},                                                                     // 0
  {33, 35, 37, 39, 41, 44, 46, 49, 52, 55, 58, 62},                         // 1
  {65, 69, 73, 78, 82, 87, 93, 98, 104, 110, 117, 123},                     // 2
  {131, 139, 147, 156, 165, 175, 185, 196, 208, 220, 233, 247},             // 3
  {262, 277, 294, 311, 330, 349, 370, 392, 415, 440, 466, 494},             // 4
  {523, 554, 587, 622, 659, 698, 740, 784, 831, 880, 932, 988},             // 5
  {1047, 1109, 1175, 1245, 1319, 1397, 1480, 1568, 1661, 1760, 1875, 1976}, // 6
  {2093, 2217, 2349, 2489, 2637, 2794, 2960, 3136, 3322, 3520, 3729, 3951}, // 7
  {4186, 4435, 4678, 4978}                                                  // 8
};

// Get the frequency of the note
static short getFreq(string note) {
  short row;
  short column;
  bool sharp = false;

  // Look if the note is sharp to select the note
  // This eases the note selection by decreasing the universe of possible answers
  if ((sharp = note[1] == 'S') == true) {
    row = note[2] - '0'; // ('x' - '0' == x) for '0' <= x <= '9'
  } else {
    row = note[1] - '0';
  }

  // Skip all computations and return the only possible result
  if  (row == 0)
    return 31;
  // Smaller case so we've less computations
  else if (row == 8) {
    switch (note[0]) {
      case 'C':
        column = SHARP(0);
        break;
      case 'D':
        column = SHARP(2);
        break;
    }
  } else {
    // Unroll the loop to ensure a speedy computation O(1) instead of O(n)
    switch (note[0]) {
      case 'C':
        column = SHARP(0);
        break;
      case 'D':
        column = SHARP(2);
        break;
      case 'E':
        column = 4;
        break;
      case 'F':
        column = SHARP(5);
        break;
      case 'G':
        column = SHARP(7);
        break;
      case 'A':
        column = SHARP(9);
        break;
      case 'B':
        column = 11;
        break;
    }
  }

  // Return the frequency
  return freqs[row][column];
}

// The frequencies are always in sorted order so we can perform a binary search
// Algorithm thanks to CLRS with the implementation taken from http://www.algolist.net/Algorithms/Binary_search
static short binarySearch(const short arr[], int value, int left, int right) {
  while (left <= right) {
    int middle = (left + right) / 2;

    if (arr[middle] == value)
      return middle;
    else if (arr[middle] > value)
      right = middle - 1;
    else
      left = middle + 1;
  }

  return -1;
}

// Get the note
static string getNote(short freq) {
  short row;

  // Skip other computations and return instantly.
  if (freq == 31)
    return "B0";
  else if (freqs[1][0] >= freq || freq <= freqs[1][11]) {
    row = 1;
  } else if (freqs[2][0] >= freq || freq <= freqs[2][11]) {
    row = 2;
  } else if (freqs[3][0] >= freq || freq <= freqs[3][11]) {
    row = 3;
  } else if (freqs[4][0] >= freq || freq <= freqs[4][11]) {
    row = 4;
  } else if (freqs[5][0] >= freq || freq <= freqs[5][11]) {
    row = 5;
  } else if (freqs[6][0] >= freq || freq <= freqs[6][11]) {
    row = 6;
  } else if (freqs[7][0] >= freq || freq <= freqs[7][11]) {
    row = 7;
  } else if (freqs[8][0] >= freq || freq <= freqs[8][3]) {
    // Slightly different computation due to the array being of a different size
    short selected = binarySearch(freqs[8], freq, 0, 3);
    return notes[8][selected];
  }

  // Do a binary search.
  short selected = binarySearch(freqs[row], freq, 0, 11);

  // Get the note
  return notes[row][selected];
}

// Functions for creating and destroying notes

// Create the note. Both can be filled in, one can be NULL but they can't both be NULL.
struct note createNote(const short *freq, const char *note, const short duration) {
  assert(!(freq == NULL && note == NULL));

  struct note temp;

  if (freq == NULL) {
    temp.note = note;
    temp.freq = getFreq(note);
  } else if (note == NULL) {
    temp.note = getNote(freq);
    temp.freq = freq;
  } else {
    temp.note = note;
    temp.freq = freq;
  }

  temp.duration = duration;

  return temp;
}
